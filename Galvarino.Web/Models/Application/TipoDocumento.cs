﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Galvarino.Web.Models.Application
{
    public enum TipoDocumento
    {
        Pagare,
        FotocopiaCedulaIdentidad,
        SolicitudCredito,
        Liquidacion,
        HojaProlongacion,
        AcuerdoDePago,
        ContratoCredito,
        CertificadoVigenciaEmpleador,
        InformeCredito,
        AnexoSeguroDesgravamen,
        AnexoSeguroCesantia,
        CertificadoDomicilio,
        AnexoEmpleador,
        CotizacionesOFondoPensiones,
        CertificadoDeudaOMandato

    }
}
